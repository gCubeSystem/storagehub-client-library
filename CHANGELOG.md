# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0]

### Feature

porting to model 2.0

## [v1.3.0] - [2021-06-18]

### Feature

method for check item existance moved to a new rest call

## [v1.2.2] - [2021-02-08]

### Feature

method for user existence check added


## [v1.2.1] - [2020-07-17]

### Feature

method for description update added
