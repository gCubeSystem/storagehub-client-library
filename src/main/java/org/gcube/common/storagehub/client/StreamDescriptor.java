package org.gcube.common.storagehub.client;

import java.io.InputStream;

public class StreamDescriptor {

	private InputStream stream;
	private String fileName;
	private String contentType;
	private long contentLenght;
	
	public StreamDescriptor(InputStream stream, String fileName, String contentType, long contentLenght) {
		super();
		this.stream = stream;
		this.fileName = fileName;
		this.contentType= contentType;
		this.contentLenght = contentLenght;
	}

	public InputStream getStream() {
		return stream;
	}

	public String getFileName() {
		return fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public long getContentLenght() {
		return contentLenght;
	}

	
}
