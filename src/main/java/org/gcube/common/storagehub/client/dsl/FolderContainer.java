package org.gcube.common.storagehub.client.dsl;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Set;

import org.gcube.common.storagehub.client.proxies.ItemManagerClient;
import org.gcube.common.storagehub.model.acls.ACL;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.GCubeItem;
import org.gcube.common.storagehub.model.plugins.PluginParameters;
import org.gcube.common.storagehub.model.types.FolderInfoType;

public class FolderContainer extends ItemContainer<FolderItem>{
			
//	private String folderPath = null;
	
	protected FolderContainer(ItemManagerClient itemclient, FolderItem item) {
		super(itemclient, item);		
	}
	
	protected FolderContainer(ItemManagerClient itemclient, String folderId) {
		super(itemclient, folderId);		
	}
	
	public ContainerType getType() {
		return ContainerType.FOLDER;
	}
	
	/**
	 * 
	 * returns the children of this {@FolderContainer}
	 * 
	 * By default this method return all FolderContainers (asContainers) or Items (asItems) without accounting data, content data and metadata of the item
	 * to add these information see {@ListResolverTyped}
	 * 
	 * @return {@ListResolverTyped}
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to read this folder
	 */
	public ListResolverTyped list() throws StorageHubException {
		return new ListResolverTyped((onlyType, includeHidden, excludes) -> itemclient.getChildren(itemId, onlyType, includeHidden, excludes), itemclient) ;
	}
	
	/**
	 * 
	 * creates a {@AbstractFileItem} inside the folder represented by this FolderContainer
	 * 
	 * @param stream the file stream
	 * @param filename the name of the item in the workspace
	 * @param description the description of the item in the workspace
	 * @return {@FileContainer} of the Item created
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FileContainer uploadFile(InputStream stream, String filename, String description) throws StorageHubException {
		return new FileContainer(itemclient, itemclient.uploadFile(stream, this.itemId , filename, description));
	}
	
	/**
	 * 
	 * creates a {@AbstractFileItem} inside the folder represented by this FolderContainer
	 * 
	 * @param stream the file stream
	 * @param filename the name of the item in the workspace
	 * @param description the description of the item in the workspace
	 * @param size the size of the stream in Bytes
	 * @return {@FileContainer} of the Item created
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FileContainer uploadFile(InputStream stream, String filename, String description, long size) throws StorageHubException {
		return new FileContainer(itemclient, itemclient.uploadFile(stream, this.itemId , filename, description, size));
	}
	
	/**
	 * 
	 * creates a {@AbstractFileItem} inside the folder represented by this FolderContainer
	 * 
	 * @param file the file
	 * @param description the description of the item in the workspace
	 * @return {@FileContainer} of the Item created
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FileContainer uploadFile(File file, String description) throws StorageHubException {
		return new FileContainer(itemclient, itemclient.uploadFile(file, this.itemId , file.getName(), description));
	}
		
	/**
	 * 
	 * creates a {@AbstractFileItem} inside the folder represented by this FolderContainer
	 * 
	 * @param uri URI of the file
	 * @param name the name of the item in the workspace
	 * @param description the description of the item in the workspace
	 * @return {@FileContainer} of the Item created
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FileContainer uploadFile(URI uri, String name, String description) throws StorageHubException {
		return new FileContainer(itemclient, itemclient.uploadFile(uri, this.itemId, name, description));
	}
	
	
	/**
	 * 
	 * creates a {@ExternalLink} inside the folder represented by this FolderContainer
	 * 
	 * @param url the url
	 * @param name the name of the item in the workspace
	 * @param description the description of the item in the workspace
	 * @return {@URLContainer} of the Item created
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public URLContainer addUrl(URL url, String name, String description) throws StorageHubException {
		return new URLContainer(itemclient, itemclient.createURL(this.itemId, name, description, url));
	}
	
	/**
	 * 
	 * creates a set of {@Item} in the workspace extracting the Archive
	 * 
	 * @param stream the file stream
	 * @param extractionFolderName the root name of the folder where the archive will be extracted (A new folder with this name will be created)
	 * @return {@FolderContainer} of the extraction Folder
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FolderContainer uploadArchive(InputStream stream, String extractionFolderName) throws StorageHubException {
		return new FolderContainer(itemclient, itemclient.uploadArchive(stream, this.itemId , extractionFolderName));
	}
	
	/**
	 * 
	 * creates a set of {@Item} in the workspace extracting the Archive
	 * 
	 * @param stream the file stream
	 * @param extractionFolderName the root name of the folder where the archive will be extracted (A new folder with this name will be created)
	 * @return {@FolderContainer} of the extraction Folder
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FolderContainer uploadArchive(InputStream stream, String extractionFolderName, long size) throws StorageHubException {
		return new FolderContainer(itemclient, itemclient.uploadArchive(stream, this.itemId , extractionFolderName, size));
	}
	
	/**
	 * 
	 * create a new {@FolderItem} inside the {@FolderItem} represented by this FolderContainer
	 * 
	 * @param name the name of the folder
	 * @param description the description of the folder
	 * @return the {@FolderContainer} representing the new folder
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FolderContainer newFolder(String name, String description) throws StorageHubException {
		String newFolderId = itemclient.createFolder(this.itemId, name, description, false);
		return new FolderContainer(itemclient, newFolderId);
	}
	
	/**
	 * 
	 * create a new {@FolderItem} inside the {@FolderItem} represented by this FolderContainer
	 * 
	 * @param name the name of the folder
	 * @param description the description of the folder
	 * @return the {@FolderContainer} representing the new folder
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FolderContainer newExternalFolder(String name, String description, boolean hidden, String storageName, PluginParameters storageParameters) throws StorageHubException {
		String newFolderId = itemclient.createFolder(this.itemId, name, description, false, storageName, storageParameters);
		return new FolderContainer(itemclient, newFolderId);
	}
	
	/**
	 * 
	 * create a new hidden {@FolderItem} inside the folder represented by this FolderContainer
	 * 
	 * @param name the name of the folder
	 * @param description the description of the folder
	 * @return the {@FolderContainer} representing the new folder
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public FolderContainer newHiddenFolder(String name, String description) throws StorageHubException {
		String newFolderId = itemclient.createFolder(this.itemId, name, description, true);
		return new FolderContainer(itemclient, newFolderId);
	}
	
	
	/**
	 * 
	 * create a new hidden Folder inside the folder represented by this FolderContainer
	 * 
	 * @param name the name of the folder
	 * @param description the description of the folder
	 * @return the {@FolderContainer} representing the new folder
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to write in this folder
	 */
	public GenericItemContainer newGcubeItem(GCubeItem item) throws StorageHubException {
		String itemId = itemclient.createGcubeItem(this.itemId, item);
		return new GenericItemContainer(itemclient, itemId);
	}
	
	/**
	 * 
	 * returns the {@ACL} of the {FolderItem} represented by this FolderContainer
	 * 
	 * @return a List of {@ACL}
	 * @throws {@StorageHubException}
	 */
	public List<ACL> getAcls() throws StorageHubException {
		return itemclient.getACL(this.itemId);
	}
	
	
	
	/**
	 * 
	 * changes {@ACL} of the {FolderItem} represented by this FolderContainer for a user
	 * 
	 * @return the {@FolderContainer}
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not administrator of this folder
	 */
	public FolderContainer changeAcls(String user, AccessType accessType) throws StorageHubException {
		itemclient.changeACL(this.itemId, user, accessType);
		return this;
	}
	
	public boolean canWrite() throws Exception {
		return itemclient.canWriteInto(this.itemId);
	}
	
	
	/**
	 * 
	 * returns the children of this {@FolderContainer} that matches the name pattern
	 * 
	 * @return {@ListResolver}
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to read this folder
	 */
	public ListResolver findByName(String namePattern) throws StorageHubException {
		return new ListResolver((onlyType, includeHidden, excludes) -> itemclient.findChildrenByNamePattern(itemId, namePattern , excludes), itemclient);
	}
	
	/**
	 * 
	 * returns the children of this {@FolderContainer} that matches the name pattern
	 * 
	 * @return {@ListResolver}
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to read this folder
	 */
	public OpenResolver openByRelativePath(String relativePath) throws StorageHubException {
		final String id = this.itemId;
		return new OpenResolver(itemclient.getByRelativePath(id, relativePath), itemclient);
	}
	
	/**
	 * 
	 * returns the items that matches the name pattern searching recursively on all subfolders filtering the trashed Items
	 * 
	 * @return {@ListResolver}
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to read this folder
	 */
	public ListResolver search(String nameTomatch, boolean excludeTrashed) throws StorageHubException {
		return new ListResolver((onlyType, includeHidden, excludes) -> itemclient.search(itemId, onlyType, includeHidden, excludeTrashed, nameTomatch, excludes), itemclient);
	}
	
	/**
	 * 
	 * returns the items that matches the name pattern searching recursively on all subfolders
	 * 
	 * @return {@ListResolver}
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to read this folder
	 */
	public ListResolver search(String nameTomatch) throws StorageHubException {
		return search(nameTomatch, true);
	}
	
	/**
	 * 
	 * shares this Folder with a list of users setting the same {@AccessType} for everyone in the list.
	 * if the folder is already shared it add the users to the share.
	 * 
	 * @return the current {@FolderContainer} updated
	 * @throws {@InvalidItemException} 
	 * @throws {@UserNotAuthorizedException} if user is not authorized to share this folder
	 */
	public FolderContainer share(Set<String> users, AccessType accessType) throws StorageHubException {
		itemclient.shareFolder(this.itemId, users, accessType);
		this.invalidateItem();
		return this;
	}
	
	/**
	 * 
	 * remove share from this Folder for a list of users, for everyone or only for the caller. Is applicable only on {@SharedFolder}.
	 * if users is empty or null unshare the entire folder for everyone.
	 * if users contains only the caller login the folder is removed only for him.
	 * 
	 * @return the current {@FolderContainer} updated
	 * @throws {@InvalidItemException} if this folder is not share
	 * @throws {@UserNotAuthorizedException} if user is not authorized to unshare this folder
	 */
	public FolderContainer unshare(Set<String> users) throws Exception {
		String unsharedId = itemclient.unshareFolder(this.itemId, users);
		return new FolderContainer(itemclient, unsharedId);
	}
	
	/**
	 * 
	 * unpublish this Folder.
	 * 
	 * @return the current {@FolderContainer} updated
	 * @throws {@InvalidCallParameter} if this is not a Folder
	 * @throws {@UserNotAuthorizedException} if user is not authorized to unpublish this folder
	 */
	public FolderContainer unpublish() throws Exception {
		itemclient.setPublic(this.itemId, false);
		this.invalidateItem();
		return this;
	}
	
	/**
	 * 
	 * makes this Folder public.
	 *
	 * @return the current {@FolderContainer} updated
	 * @throws {@InvalidCallParameter} if this is not a Folder
	 * @throws {@UserNotAuthorizedException} if user is not authorized to publish this folder
	 */
	public FolderContainer publish() throws Exception {
		itemclient.setPublic(this.itemId, true);
		this.invalidateItem();
		return this;
	}
	
	/**
	 * 
	 * retrieve the folder info (item count and size)
	 *
	 * @return the {@FolderInfoType}
	 * @throws {@InvalidCallParameter} if this is not a Folder
	 * @throws {@UserNotAuthorizedException} if user is not authorized to get the info
	 */
	public FolderInfoType info() throws Exception {
		return itemclient.getFolderInfo(this.itemId);
	}
	
}
