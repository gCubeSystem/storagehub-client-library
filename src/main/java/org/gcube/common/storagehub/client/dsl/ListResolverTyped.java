package org.gcube.common.storagehub.client.dsl;

import org.gcube.common.storagehub.client.proxies.ItemManagerClient;
import org.gcube.common.storagehub.model.items.Item;

public class ListResolverTyped extends ListResolver {

	protected ListResolverTyped(ListRetriever retriever, ItemManagerClient itemClient) {
		super(retriever, itemClient);
	}

	/**
	 * 
	 * Filter the items returned by the given type
	 * 
	 * @param type Item class
	 * @return {@ListResolver}
	 */
	public ListResolver ofType(Class<? extends Item> type){
		onlyType = type;
		return this;
	}
	
	
	/**
	 * 
	 * Includes hidden items
	 *  
	 * @return {@ListResolver}
	 */	
	public ListResolver includeHidden(){
		includeHidden = true;
		return this;
	}
	
}
