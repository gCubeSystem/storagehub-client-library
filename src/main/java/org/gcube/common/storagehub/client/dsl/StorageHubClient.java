package org.gcube.common.storagehub.client.dsl;

import java.io.File;
import java.io.InputStream;
import java.net.URI;

import org.gcube.common.storagehub.client.plugins.AbstractPlugin;
import org.gcube.common.storagehub.client.proxies.GroupManagerClient;
import org.gcube.common.storagehub.client.proxies.ItemManagerClient;
import org.gcube.common.storagehub.client.proxies.UserManagerClient;
import org.gcube.common.storagehub.client.proxies.WorkspaceManagerClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;

public class StorageHubClient {

	protected WorkspaceManagerClient wsClient = AbstractPlugin.workspace().build();
	protected ItemManagerClient itemclient = AbstractPlugin.item().build();
	protected GroupManagerClient groupClient = AbstractPlugin.groups().build();
	protected UserManagerClient userClient = AbstractPlugin.users().build();
		
	public StorageHubClient() {}
		
	public StorageHubClient(URI uri) {
		wsClient = AbstractPlugin.workspace().at(uri).build();
		itemclient = AbstractPlugin.item().at(uri).build();
		groupClient = AbstractPlugin.groups().at(uri).build();
		userClient = AbstractPlugin.users().at(uri).build();
	}
	
	public void impersonate(String user){
		wsClient.impersonate(user);
		itemclient.impersonate(user);
	}
	
	public FolderContainer getWSRoot(){
		return new FolderContainer(itemclient, wsClient.getWorkspace());
	}
	
/*	public OpenResolver open(Path relativePath) {
		Item item = wsClient.retieveItemByPath(relativePath.toPath());
		return new OpenResolver(item, itemclient);		
	}*/
	
	public OpenResolver open(String id) throws StorageHubException{
		Item item = itemclient.get(id);
		return new OpenResolver(item, itemclient);	
	}
	
	public FolderContainer openVREFolder() {
		return new FolderContainer(itemclient, wsClient.getVreFolder());
	}
			
	public ListResolver getVREFolders() {
		return new ListResolver((onlyType, includeHidden, excludes) -> wsClient.getVreFolders(excludes), itemclient);
	}
	
	public VREFolderManager getVreFolderManager(String vreTitle) {
		return new VREFolderManager(wsClient, groupClient, vreTitle);
	}
	
	public VREFolderManager getVreFolderManager() {
		return new VREFolderManager(wsClient, groupClient, null);
	}
	
	public FolderContainer openTrash() {
		return new FolderContainer(itemclient, wsClient.getTrashFolder());
	}
	
	public void emptyTrash() throws StorageHubException{
		wsClient.emptyTrash();
	}
	
	public long getTotalVolume() {
		return wsClient.getTotalVolume();
	}
	
	public long getTotalItemCount() {
		return wsClient.getTotalItemCount();
	}
			
	public GenericItemContainer restoreThrashItem(String trashItemId) throws StorageHubException {
		return new GenericItemContainer(itemclient, wsClient.restoreFromTrash(trashItemId, null));
	}
	
	public GenericItemContainer restoreThrashItem(String trashItemId, String destinationFolderId) throws StorageHubException {
		return new GenericItemContainer(itemclient, wsClient.restoreFromTrash(trashItemId, destinationFolderId));
	}
	
	public void createUserAccount(String userId) throws StorageHubException {
		userClient.createUser(userId);
	}
	
	public void deleteUserAccount(String userId) throws StorageHubException {
		userClient.removeUser(userId);
	}
	
	public boolean userExists(String user) throws StorageHubException {
		return userClient.exists(user);
	}
	
	public String uploadFileOnVolatile(File file) throws StorageHubException {
		return wsClient.uploadFileToVolatile(file);
	}
	
	public String uploadFileOnVolatile(InputStream stream, String fileName) throws StorageHubException {
		return wsClient.uploadFileToVolatile(stream, fileName);
	}
	
	public String uploadFileOnVolatile(InputStream stream, String fileName, long size) throws StorageHubException {
		return wsClient.uploadFileToVolatile(stream, fileName, size);
	}
	
}
