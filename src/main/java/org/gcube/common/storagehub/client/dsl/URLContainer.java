package org.gcube.common.storagehub.client.dsl;

import org.gcube.common.storagehub.client.proxies.ItemManagerClient;
import org.gcube.common.storagehub.model.items.ExternalLink;

public class URLContainer extends ItemContainer<ExternalLink> {

	protected URLContainer(ItemManagerClient itemclient, ExternalLink item) {
		super(itemclient, item);
	}

	protected URLContainer(ItemManagerClient itemclient, String itemId) {
		super(itemclient, itemId);		
	}
	
	public ContainerType getType() {
		return ContainerType.URL;
	}

}
