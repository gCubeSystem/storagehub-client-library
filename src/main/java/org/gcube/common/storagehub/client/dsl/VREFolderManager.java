package org.gcube.common.storagehub.client.dsl;

import java.util.List;

import org.gcube.common.storagehub.client.proxies.GroupManagerClient;
import org.gcube.common.storagehub.client.proxies.WorkspaceManagerClient;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;

public class VREFolderManager {

	//private WorkspaceManagerClient wsClient;
	private GroupManagerClient groupClient;
	
	private String vreTitle;
	
	protected VREFolderManager(WorkspaceManagerClient wsClient, GroupManagerClient groupClient, String vreTitle) {
		super();
		//this.wsClient = wsClient;
		this.groupClient = groupClient;
		this.vreTitle = vreTitle;
	}

	public void removeUser(String userId) throws StorageHubException{
		groupClient.removeUserFromGroup(userId, vreTitle);
	}
	
	public void addUser(String userId) throws StorageHubException{
		groupClient.addUserToGroup(userId, vreTitle);
	}
	
	public List<String> getUsers() throws StorageHubException{
		return groupClient.getUsersOfGroup(vreTitle);
	}
		
	/*private Item getVreFolder() {
		return wsClient.getVreFolder(Excludes.ALL.toArray(new String[0]));
	}*/
	
	public void setAdmin(String userId) throws StorageHubException{
		groupClient.addAdmin(vreTitle, userId);
	}

	public void removeAdmin(String userId) throws StorageHubException{
		groupClient.removeAdmin(vreTitle, userId);
	}
	
	public List<String> getAdmins() throws StorageHubException{
		return groupClient.getAdmins(vreTitle);
	}

	public void createVRE(AccessType accessType, String folderOwner) throws StorageHubException{
		groupClient.createGroup(vreTitle, accessType, folderOwner);
	}
	
	public void removeVRE() throws StorageHubException{
		groupClient.removeGroup(vreTitle);
	}
	
}
