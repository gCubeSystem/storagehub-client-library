package org.gcube.common.storagehub.client.plugins;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.gcube.common.clients.Plugin;
import org.gcube.common.clients.ProxyBuilder;
import org.gcube.common.clients.ProxyBuilderImpl;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.storagehub.client.Constants;
import org.gcube.common.storagehub.client.proxies.GroupManagerClient;
import org.gcube.common.storagehub.client.proxies.ItemManagerClient;
import org.gcube.common.storagehub.client.proxies.MessageManagerClient;
import org.gcube.common.storagehub.client.proxies.UserManagerClient;
import org.gcube.common.storagehub.client.proxies.WorkspaceManagerClient;



public abstract class AbstractPlugin<S,P> implements Plugin<S,P> {

		
	private static final ItemManagerPlugin item_plugin = new ItemManagerPlugin();
	private static final WorkspaceManagerPlugin workspace_plugin = new WorkspaceManagerPlugin();
	private static final UserManagerPlugin user_plugin = new UserManagerPlugin();
	private static final GroupManagerPlugin group_plugin = new GroupManagerPlugin();
	private static final MessageManagerPlugin messages_plugin = new MessageManagerPlugin();
	
	
	public static ProxyBuilder<ItemManagerClient> item() {
	    return new ProxyBuilderImpl<GXWebTargetAdapterRequest,ItemManagerClient>(item_plugin);
	}
	
	public static ProxyBuilder<GroupManagerClient> groups() {
		
	    return new ProxyBuilderImpl<GXWebTargetAdapterRequest,GroupManagerClient>(group_plugin);
	}
	
	public static ProxyBuilder<UserManagerClient> users() {
	    return new ProxyBuilderImpl<GXWebTargetAdapterRequest,UserManagerClient>(user_plugin);
	}
	
	public static ProxyBuilder<WorkspaceManagerClient> workspace() {
	    return new ProxyBuilderImpl<GXWebTargetAdapterRequest,WorkspaceManagerClient>(workspace_plugin);
	}
		
	public static ProxyBuilder<MessageManagerClient> messages() {
	    return new ProxyBuilderImpl<GXWebTargetAdapterRequest,MessageManagerClient>(messages_plugin);
	}
	
	public final String name;
	
	protected List<Class<?>> customClasses = Collections.emptyList();
	
	public AbstractPlugin(String name) {
		this.name=name;
	}
	
	@Override
	public String serviceClass() {
		return Constants.SERVICE_CLASS;
	}
	
	@Override
	public String serviceName() {
		return Constants.SERVICE_NAME;
	}
	
	@Override
	public String namespace() {
		return Constants.NAMESPACE;
	}
	
	@Override
	public String name() {
		return name;
	}
	
	public void register(Class<?> ... _classes) {
		customClasses = Arrays.asList(_classes);		
	}
}