package org.gcube.common.storagehub.client.proxies;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

public class DefaultGroupManager extends DefaultManagerClient implements GroupManagerClient {
	
	
	public DefaultGroupManager(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		super(delegate);
	}

		
	@Override
	public void addUserToGroup(String userId, String groupId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("userId", userId);
				
				GXInboundResponse response = myManager.path(groupId).path("users").put(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return null;
			}
		};
		try {
			delegate.make(call);
			return;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public void removeUserFromGroup(String userId, String groupId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				GXInboundResponse response = myManager.path(groupId).path("users").path(userId).delete();
				
				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError();
				}

				return null;
			}
		};
		try {
			delegate.make(call);
			return;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}

	}

	@Override
	public List<String> getGroups() throws StorageHubException {
		Call<GXWebTargetAdapterRequest, List<String>> call = new Call<GXWebTargetAdapterRequest,  List<String>>() {
			@Override
			public  List<String> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				GXInboundResponse response = myManager.get();
				
				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return response.getSource().readEntity(List.class);
			}
		};
		try {
			List<String> users = delegate.make(call);
			return users;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public void removeGroup(String groupId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				GXInboundResponse response = myManager.path(groupId).delete();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return null;
			}
		};
		try {
			delegate.make(call);
			return;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}

		
	}

	@Override
	public void createGroup(String groupId, AccessType accessType, String folderOwner) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				try (FormDataMultiPart multipart = new FormDataMultiPart()){
					multipart.field("accessType", accessType, MediaType.APPLICATION_JSON_TYPE);
					multipart.field("group", groupId);
					multipart.field("folderOwner", folderOwner);
					
					
					GXInboundResponse response = myManager.post(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA_TYPE));

					if (response.isErrorResponse()) {
						if (response.hasException()) 
							throw response.getException();
						else if (response.getHTTPCode()==403)
								throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
						else
							throw new BackendGenericError();
					}

	
				}
						
				return null;
			}
		};
		try {
			delegate.make(call);
			return;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
		
	}

	@Override
	public List<String> getUsersOfGroup(String groupId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, List<String>> call = new Call<GXWebTargetAdapterRequest,  List<String>>() {
			@Override
			public  List<String> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				GXInboundResponse response = myManager.path(groupId).path("users").get();
				
				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this methdo is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return response.getSource().readEntity(List.class);
			}
		};
		try {
			List<String> users = delegate.make(call);
			return users;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	
	
	@Override
	public void addAdmin(String groupId, String userId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("userId", userId);
				
				GXInboundResponse response = myManager.path(groupId).path("admins").put(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this methdo is not allowed for the user");
					else
						throw new BackendGenericError();
				}

			
				return null;
			}
		};
		try {
			delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public void removeAdmin(String groupId, String userId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				GXInboundResponse response = myManager.path(groupId).path("admins").path(userId).delete();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this methdo is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return null;
			}
		};
		try {
			delegate.make(call);
			return;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
		
	}

	@Override
	public List<String> getAdmins(String groupId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, List<String>> call = new Call<GXWebTargetAdapterRequest,  List<String>>() {
			@Override
			public  List<String> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				GXInboundResponse response = myManager.path(groupId).path("admins").get();
				
				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this methdo is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return response.getSource().readEntity(List.class);
			}
		};
		try {
			List<String> users = delegate.make(call);
			return users;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

}
