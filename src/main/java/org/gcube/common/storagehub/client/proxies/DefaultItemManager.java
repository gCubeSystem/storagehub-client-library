package org.gcube.common.storagehub.client.proxies;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.storagehub.client.StreamDescriptor;
import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.acls.ACL;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.annotations.RootNode;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.GCubeItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.plugins.PluginParameters;
import org.gcube.common.storagehub.model.service.ItemList;
import org.gcube.common.storagehub.model.service.ItemWrapper;
import org.gcube.common.storagehub.model.service.Version;
import org.gcube.common.storagehub.model.service.VersionList;
import org.gcube.common.storagehub.model.types.ACLList;
import org.gcube.common.storagehub.model.types.FolderInfoType;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

public class DefaultItemManager extends DefaultManagerClient implements ItemManagerClient {

	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String CONTENT_LENGHT = "Content-Length";

	public DefaultItemManager(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		super(delegate);
	}

	@Override
	public StreamDescriptor resolvePublicLink(String identifier) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, StreamDescriptor> call = new Call<GXWebTargetAdapterRequest, StreamDescriptor>() {
			@Override
			public StreamDescriptor call(GXWebTargetAdapterRequest manager) throws StorageHubException, Exception {
				Objects.requireNonNull(identifier, "id cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path("publiclink").path(identifier);
				Map<String, Object[]> params = initQueryParameters();

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				Response resp = response.getSource();

				return createStreamDescriptor(resp);
			}

		};
		try {
			StreamDescriptor result = delegate.make(call);
			return result;
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	@Deprecated
	public List<? extends Item> getChildren(String id, Class<? extends Item> onlyOfType, String... excludeNodes)
			throws StorageHubException {
		return getChildren(id, onlyOfType, false, excludeNodes);
	}

	@Override
	public List<? extends Item> getChildren(String id, Class<? extends Item> onlyOfType, boolean showHidden,
			String... excludeNodes) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws StorageHubException, Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("children");
				Map<String, Object[]> params = initQueryParameters();
				if (excludeNodes != null && excludeNodes.length > 0)
					params.put("exclude", excludeNodes);

				if (onlyOfType != null)
					params.put("onlyType", new Object[] { resolveNodeType(onlyOfType) });
				params.put("showHidden", new Object[] { showHidden });

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				ItemList items = response.getSource().readEntity(ItemList.class);

				return items;
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<? extends Item> getChildren(String id, int start, int limit, boolean showHidden,
			Class<? extends Item> onlyOfType, String... excludeNodes) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				try {
					GXWebTargetAdapterRequest myManager = manager.path(id).path("children").path("paged");
					Map<String, Object[]> params = initQueryParameters();

					if (excludeNodes != null && excludeNodes.length > 0)
						params.put("exclude", excludeNodes);

					if (onlyOfType != null)
						params.put("onlyType", new Object[] { resolveNodeType(onlyOfType) });

					params.put("start", new Object[] { start });
					params.put("limit", new Object[] { limit });
					params.put("showHidden", new Object[] { showHidden });

					GXInboundResponse response = myManager.queryParams(params).get();

					if (response.isErrorResponse()) {
						if (response.hasException())
							throw response.getException();
						else
							throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
					}

					ItemList items = response.getSource().readEntity(ItemList.class);

					return items;
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<? extends Item> getChildren(String id, int start, int limit, boolean showHidden, String... excludeNodes)
			throws StorageHubException {
		return getChildren(id, start, limit, showHidden, null, excludeNodes);
	}

	@Override
	@Deprecated
	public List<? extends Item> getChildren(String id, String... excludeNodes) throws StorageHubException {
		return getChildren(id, false, excludeNodes);
	}

	@Override
	public List<? extends Item> getChildren(String id, boolean showHidden, String... excludeNodes)
			throws StorageHubException {
		return getChildren(id, null, showHidden, excludeNodes);
	}

	public List<? extends Item> search(String id, Class<? extends Item> onlyOfType, boolean showHidden,
			boolean excludeTrashed, String nameToMatch, String... excludeNodes) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws StorageHubException, Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("search");
				Map<String, Object[]> params = initQueryParameters();
				if (excludeNodes != null && excludeNodes.length > 0)
					params.put("exclude", excludeNodes);

				if (onlyOfType != null)
					params.put("onlyType", new Object[] { resolveNodeType(onlyOfType) });
				params.put("showHidden", new Object[] { showHidden });
				params.put("name", new String[] { URLEncoder.encode(nameToMatch, "UTF-8") });
				params.put("excludeTrashed", new Object[] { excludeTrashed });

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				ItemList items = response.getSource().readEntity(ItemList.class);

				return items;
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	@Deprecated
	public Integer childrenCount(String id, Class<? extends Item> onlyOfType) throws StorageHubException {
		return childrenCount(id, false, onlyOfType);
	}

	@Override
	public Integer childrenCount(String id, boolean showHidden, Class<? extends Item> onlyOfType)
			throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Integer> call = new Call<GXWebTargetAdapterRequest, Integer>() {
			@Override
			public Integer call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("children").path("count");
				Map<String, Object[]> params = initQueryParameters();
				if (onlyOfType != null)
					params.put("onlyType", new Object[] { resolveNodeType(onlyOfType) });

				params.put("showHidden", new Object[] { showHidden });

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(Integer.class);
			}
		};
		try {
			Integer result = delegate.make(call);
			return result;
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	@Deprecated
	public Integer childrenCount(String id) throws StorageHubException {
		return childrenCount(id, false);
	}

	@Override
	public Integer childrenCount(String id, boolean showHidden) throws StorageHubException {
		return childrenCount(id, showHidden, null);
	}

	private String resolveNodeType(Class<? extends Item> itemClass) throws StorageHubException {
		if (!itemClass.isAnnotationPresent(RootNode.class))
			return null;
		String nodeType = itemClass.getAnnotation(RootNode.class).value()[0];
		return nodeType;
	}

	@Override
	public StreamDescriptor download(String id, String... excludeNodes) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, StreamDescriptor> call = new Call<GXWebTargetAdapterRequest, StreamDescriptor>() {
			@Override
			public StreamDescriptor call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("download");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes != null && excludeNodes.length > 0)
					params.put("exclude", excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				Response resp = response.getSource();

				return createStreamDescriptor(resp);
			}
		};
		try {
			StreamDescriptor result = delegate.make(call);
			return result;
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public StreamDescriptor downloadSpecificVersion(String id, String version) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, StreamDescriptor> call = new Call<GXWebTargetAdapterRequest, StreamDescriptor>() {
			@Override
			public StreamDescriptor call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(version, "version cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).queryParams(initQueryParameters())
						.path("versions").path(version).path("download");

				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				Response resp = response.getSource();

				return createStreamDescriptor(resp);
			}
		};
		try {
			StreamDescriptor result = delegate.make(call);
			return result;
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<Version> getFileVersions(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, VersionList> call = new Call<GXWebTargetAdapterRequest, VersionList>() {
			@Override
			public VersionList call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("versions");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				VersionList versions = response.getSource().readEntity(VersionList.class);

				return versions;
			}
		};
		try {
			VersionList result = delegate.make(call);
			return result.getItemlist();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public void deleteFileVersion(String id, String versionName) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(versionName, "version name cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("versions").path(versionName);

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).delete();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return null;
			}
		};
		try {
			delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public Item get(String id, String... excludeNodes) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemWrapper<Item>> call = new Call<GXWebTargetAdapterRequest, ItemWrapper<Item>>() {
			@Override
			public ItemWrapper<Item> call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id);
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes != null && excludeNodes.length > 0)
					params.put("exclude", excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				ItemWrapper<Item> item = response.getSource().readEntity(ItemWrapper.class);

				return item;
			}
		};
		try {
			ItemWrapper<Item> result = delegate.make(call);
			return result.getItem();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}
	
	@Override
	public FolderInfoType getFolderInfo(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, FolderInfoType> call = new Call<GXWebTargetAdapterRequest, FolderInfoType>() {
			@Override
			public FolderInfoType call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("info");

				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				FolderInfoType folderInfo = response.getSource().readEntity(FolderInfoType.class);

				return folderInfo;
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public Item getByRelativePath(String id, String path, String... excludeNodes) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemWrapper<Item>> call = new Call<GXWebTargetAdapterRequest, ItemWrapper<Item>>() {
			@Override
			public ItemWrapper<Item> call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(path, "path cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("path");
				Map<String, Object[]> params = initQueryParameters();

				params.put("path", new String[] { path });

				if (excludeNodes != null && excludeNodes.length > 0)
					params.put("exclude", excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				ItemWrapper<Item> item = response.getSource().readEntity(ItemWrapper.class);

				return item;
			}
		};
		try {
			ItemWrapper<Item> result = delegate.make(call);
			return result.getItem();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public Item getRootSharedFolder(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemWrapper<Item>> call = new Call<GXWebTargetAdapterRequest, ItemWrapper<Item>>() {
			@Override
			public ItemWrapper<Item> call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).queryParams(initQueryParameters())
						.path("rootSharedFolder");
				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				ItemWrapper<Item> item = response.getSource().readEntity(ItemWrapper.class);

				return item;
			}
		};
		try {
			ItemWrapper<Item> result = delegate.make(call);
			return result.getItem();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public URL getPublickLink(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, URL> call = new Call<GXWebTargetAdapterRequest, URL>() {
			@Override
			public URL call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id).path("publiclink");
				GXInboundResponse response = myManager.queryParams(initQueryParameters()).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				URL item = response.getSource().readEntity(URL.class);

				return item;
			}
		};
		try {
			URL result = delegate.make(call);
			return result;
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public URL getPublickLink(String id, String version) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, URL> call = new Call<GXWebTargetAdapterRequest, URL>() {
			@Override
			public URL call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(version, "version cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("publiclink");
				Map<String, Object[]> params = initQueryParameters();
				params.put("version", new Object[] { version });
				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				URL item = response.getSource().readEntity(URL.class);

				return item;
			}
		};
		try {
			URL result = delegate.make(call);
			return result;
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<? extends Item> findChildrenByNamePattern(String id, String name, String... excludeNodes)
			throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(id, "name cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("items");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes != null && excludeNodes.length > 0)
					params.put("exclude", excludeNodes);
				params.put("name", new Object[] { name });

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				ItemList items = response.getSource().readEntity(ItemList.class);

				return items;
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	@Deprecated
	public String uploadFile(InputStream stream, String parentId, String fileName, String description)
			throws StorageHubException {
		return this.uploadFile(stream, parentId, fileName, description, -1);
	}

	@Override
	public String uploadFile(InputStream stream, String parentId, String fileName, String description,
			long contentLength) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(stream, "stream cannot be null");
				Objects.requireNonNull(parentId, "parentId cannot be null");
				Objects.requireNonNull(fileName, "parentId cannot be null");
				Objects.requireNonNull(description, "parentId cannot be null");

				GXWebTargetAdapterRequest myManager = manager.register(MultiPartFeature.class).path(parentId)
						.path("create").path("FILE");

				GXInboundResponse response = null;
				FormDataMultiPart multipart = new FormDataMultiPart();

				multipart.field("name", fileName);
				multipart.field("description", description);

				StreamDataBodyPart filePart = new StreamDataBodyPart("file", stream);
				filePart.setContentDisposition(
						FormDataContentDisposition.name("file").fileName(fileName).size(contentLength).build());

				multipart.bodyPart(filePart);

				response = myManager.queryParams(initQueryParameters())
						.post(Entity.entity(multipart, multipart.getMediaType()));

				if (response.isErrorResponse()) {
					if (response.hasException()) {
						throw response.getException();
					} else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String uploadFile(File file, String parentId, String fileName, String description)
			throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(file, "file cannot be null");
				Objects.requireNonNull(parentId, "parentId cannot be null");
				Objects.requireNonNull(fileName, "parentId cannot be null");
				Objects.requireNonNull(description, "parentId cannot be null");

				GXWebTargetAdapterRequest myManager = manager.register(MultiPartFeature.class).path(parentId)
						.path("create").path("FILE");

				GXInboundResponse response = null;
				FormDataMultiPart multipart = new FormDataMultiPart();

				multipart.field("name", fileName);
				multipart.field("description", description);

				BodyPart fdp = new FileDataBodyPart("file", file);

				multipart.bodyPart(fdp);

				Entity<FormDataMultiPart> entity = Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA);

				response = myManager.queryParams(initQueryParameters()).post(entity);

				if (response.isErrorResponse()) {
					if (response.hasException()) {
						throw response.getException();
					} else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String uploadFile(URI uri, String parentId, String fileName, String description) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(uri, "uri cannot be null");
				Objects.requireNonNull(parentId, "parentId cannot be null");
				Objects.requireNonNull(fileName, "parentId cannot be null");
				Objects.requireNonNull(description, "parentId cannot be null");

				GXWebTargetAdapterRequest myManager = manager.register(MultiPartFeature.class).path(parentId)
						.path("create").path("FILE");

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("url", uri.toString());
				formData.add("name", fileName);
				formData.add("description", description);

				GXInboundResponse response = myManager.queryParams(initQueryParameters())
						.post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

				if (response.isErrorResponse()) {
					if (response.hasException()) {
						throw response.getException();
					} else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String uploadArchive(InputStream stream, String parentId, String extractionFolderName, long contentLength)
			throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(stream, "stream cannot be null");
				Objects.requireNonNull(parentId, "parentId cannot be null");
				Objects.requireNonNull(extractionFolderName, "extraction folder name cannot be null");

				GXWebTargetAdapterRequest myManager = manager.register(MultiPartFeature.class).path(parentId)
						.path("create").path("ARCHIVE");

				FormDataMultiPart multipart = new FormDataMultiPart();
				multipart.field("parentFolderName", extractionFolderName);
				multipart.field("file", stream, MediaType.APPLICATION_OCTET_STREAM_TYPE);

				if (contentLength > 0)
					myManager = myManager.header("Content-Length", contentLength);

				GXInboundResponse response = myManager.queryParams(initQueryParameters())
						.post(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA_TYPE));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);

			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String uploadArchive(InputStream stream, String parentId, String extractionFolderName)
			throws StorageHubException {
		return this.uploadArchive(stream, parentId, extractionFolderName, 0);
	}

	@Override
	@Deprecated
	public String createFolder(String parentId, String name, String description) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				return createFolder(parentId, name, description, false);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String createFolder(String parentId, String name, String description, boolean hidden, String pluginName,
			PluginParameters parameters) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(parentId, "parentId cannot be null");
				Objects.requireNonNull(name, "folder name cannot be null");
				Objects.requireNonNull(description, "parentId cannot be null");

				GXWebTargetAdapterRequest myManager;
				if (pluginName == null)
					myManager = manager.path(parentId).path("create").path("FOLDER");
				else 
					myManager = manager.path(parentId).path("create").path("EXTERNALFOLDER");
				
				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("name", name);
				formData.add("description", description);
				formData.add("hidden", Boolean.toString(hidden));

				if (pluginName!=null) {
					formData.add("pluginName", pluginName);
					if (parameters!=null) 
						parameters.getParameters().forEach((k,v) -> formData.add(String.format("plugin.%s",k),(String)v));
					
				}
				
				GXInboundResponse response = myManager.queryParams(initQueryParameters())
						.post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	public String createFolder(String parentId, String name, String description, boolean hidden)
			throws StorageHubException {
		return createFolder(parentId, name, description, hidden, null, null);
	}

	@Override
	public String createURL(String parentId, String name, String description, URL url) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(parentId, "parentId cannot be null");
				Objects.requireNonNull(name, "folder name cannot be null");
				Objects.requireNonNull(description, "parentId cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(parentId).path("create").path("URL");

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("name", name);
				formData.add("description", description);
				formData.add("value", url.toString());

				GXInboundResponse response = myManager.queryParams(initQueryParameters())
						.post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String createGcubeItem(String parentId, GCubeItem item) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {

				Objects.requireNonNull(parentId, "parentId cannot be null");
				Objects.requireNonNull(item, "item cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(parentId).path("create").path("GCUBEITEM");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).post(Entity.json(item));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String shareFolder(String id, Set<String> users, AccessType accessType) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {

				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(users, "users list cannot be null");
				Objects.requireNonNull(accessType, "access type cannot be null");

				GXWebTargetAdapterRequest myManager = manager.register(MultiPartFeature.class).path(id).path("share");

				try (FormDataMultiPart multipart = new FormDataMultiPart()) {
					multipart.field("defaultAccessType", accessType, MediaType.APPLICATION_JSON_TYPE);
					multipart.field("users", users, MediaType.APPLICATION_JSON_TYPE);

					GXInboundResponse response = myManager.queryParams(initQueryParameters())
							.put(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA_TYPE));

					if (response.isErrorResponse()) {
						if (response.hasException())
							throw response.getException();
						else
							throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
					}

					return response.getSource().readEntity(String.class);
				}
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String unshareFolder(String id, Set<String> users) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(users, "users list cannot be null");

				GXWebTargetAdapterRequest myManager = manager.register(MultiPartFeature.class).path(id).path("unshare");

				try (FormDataMultiPart multipart = new FormDataMultiPart()) {
					multipart.field("users", users, MediaType.APPLICATION_JSON_TYPE);
					GXInboundResponse response = myManager.queryParams(initQueryParameters())
							.put(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA_TYPE));

					if (response.isErrorResponse()) {
						if (response.hasException())
							throw response.getException();
						else
							throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
					}

					return response.getSource().readEntity(String.class);
				}
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	@Deprecated
	public void delete(String id) throws StorageHubException {
		delete(id, false);
	}

	@Override
	public void delete(String id, boolean force) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");

				Map<String, Object[]> queryParam = initQueryParameters();
				queryParam.put("force", new Object[] { force });

				GXWebTargetAdapterRequest myManager = manager.path(id);
				GXInboundResponse response = myManager.queryParams(queryParam).delete();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());

				}

				return null;
			}
		};
		try {
			delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<? extends Item> getAnchestors(String id, String... excludeNodes) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("anchestors");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes != null && excludeNodes.length > 0)
					params.put("exclude", excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(ItemList.class);
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<ACL> getACL(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, ACLList> call = new Call<GXWebTargetAdapterRequest, ACLList>() {
			@Override
			public ACLList call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");

				GXWebTargetAdapterRequest myManager = manager.queryParams(initQueryParameters()).path(id).path("acls");
				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(ACLList.class);
			}
		};
		try {
			return delegate.make(call).getAcls();
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public boolean canWriteInto(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Boolean> call = new Call<GXWebTargetAdapterRequest, Boolean>() {
			@Override
			public Boolean call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");

				GXWebTargetAdapterRequest myManager = manager.queryParams(initQueryParameters()).path(id).path("acls")
						.path("write");

				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(Boolean.class);

			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String changeACL(String id, String user, AccessType accessType) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(user, "user cannot be null");

				GXWebTargetAdapterRequest myManager = manager.queryParams(initQueryParameters()).path(id).path("acls");

				try (FormDataMultiPart multipart = new FormDataMultiPart()) {
					multipart.field("access", accessType, MediaType.APPLICATION_JSON_TYPE);
					multipart.field("user", user);

					GXInboundResponse response = myManager
							.put(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA_TYPE));

					if (response.isErrorResponse()) {
						if (response.hasException())
							throw response.getException();
						else
							throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
					}

					return response.getSource().readEntity(String.class);
				}

			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String removeAdmin(String id, String user) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(user, "user cannot be null");

				GXWebTargetAdapterRequest myManager = manager.queryParams(initQueryParameters()).path(id).path("acls")
						.path(user);

				GXInboundResponse response = myManager.delete();

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);

			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String copy(String id, String destinationFolderId, String newFilename) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {

				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(newFilename, "new file name cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("copy");

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("destinationId", destinationFolderId);
				formData.add("fileName", newFilename);

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.form(formData));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String setPublic(String id, boolean publish) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("publish");

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("publish", Boolean.toString(publish));

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.form(formData));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String move(String id, String destinationFolderId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(destinationFolderId, "destination folder cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("move");

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("destinationId", destinationFolderId);

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.form(formData));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String rename(String id, String newName) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(newName, "new name cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("rename");

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("newName", newName);

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.form(formData));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String setHidden(String id, Boolean value) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(value, "value cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("hidden");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.json(value));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String setDescription(String id, String description) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(description, "description cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("description");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.json(description));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String setMetadata(String id, Metadata metadata) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				Objects.requireNonNull(metadata, "metadata cannot be null");

				GXWebTargetAdapterRequest myManager = manager.path(id).path("metadata");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.json(metadata));

				if (response.isErrorResponse()) {
					if (response.hasException())
						throw response.getException();
					else
						throw new BackendGenericError("HTTP error code is " + response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		} catch (StorageHubException e) {
			throw e;
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	private StreamDescriptor createStreamDescriptor(Response resp) {
		InputStream stream = resp.readEntity(InputStream.class);
		String disposition = resp.getHeaderString(CONTENT_DISPOSITION);
		String fileName = disposition.replaceFirst("attachment; filename = ([^/s]+)?", "$1");
		String contentType = resp.getHeaderString(CONTENT_TYPE);
		long contentLenght = -1l;
		try {
			contentLenght = Long.parseLong(resp.getHeaderString(CONTENT_LENGHT));
		} catch (Exception e) {
			// DO NOTHING
		}
		return new StreamDescriptor(stream, fileName, contentType, contentLenght);
	}

}
