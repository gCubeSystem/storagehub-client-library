package org.gcube.common.storagehub.client.proxies;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.storagehub.client.plugins.AbstractPlugin;

public abstract class DefaultManagerClient implements ManagerClient{

	protected ProxyDelegate<GXWebTargetAdapterRequest> delegate;

	private String impersonedUser= null;
	
	public DefaultManagerClient(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		this.delegate = delegate;
	}

	@Override
	public void register(Class<?> _classes) {
		((AbstractPlugin<?,?>) delegate.config().plugin()).register(_classes);
	}

	@Override
	public void impersonate(String user) {
		this.impersonedUser = user;
	}

	protected Map<String, Object[]> initQueryParameters() {
		Map<String, Object[]> params = new HashMap<String, Object[]>();
		if (impersonedUser != null)
			params.put("impersonate", new Object[] {impersonedUser});
		return params;
	}
}
