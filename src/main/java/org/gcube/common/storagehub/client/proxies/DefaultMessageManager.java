package org.gcube.common.storagehub.client.proxies;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.messages.Message;
import org.gcube.common.storagehub.model.service.ItemList;
import org.gcube.common.storagehub.model.types.MessageList;

public class DefaultMessageManager extends DefaultManagerClient implements MessageManagerClient {

	
	public DefaultMessageManager(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		super(delegate);
	}

	@Override
	public Message get(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Message> call = new Call<GXWebTargetAdapterRequest, Message>() {
			@Override
			public Message call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id);

				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				Message item = response.getSource().readEntity(Message.class);


				return item;
			}
		};
		try {
			return delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<Message> getReceivedMessages(int reduceBody) throws StorageHubException {
		return getMessages("inbox", reduceBody);
	}

	@Override
	public List<Message> getSentMessages(int reduceBody) throws StorageHubException {
		return getMessages("sent", reduceBody);
	}

	@Override
	public List<Message> getReceivedMessages() throws StorageHubException {
		return getMessages("inbox", -1);
	}


	@Override
	public List<Message> getSentMessages() throws StorageHubException {
		return getMessages("sent", -1);
	}

	@Override
	public void setRead(String id, Boolean value) throws StorageHubException {
		setBooleanProp("hl:read", value, id);
	}

	@Override
	public void setOpened(String id, Boolean value) throws StorageHubException {
		setBooleanProp("hl:open", value, id);
	}

	
	private void setBooleanProp(String prop, Boolean bool, String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path(id).path(prop);

				GXInboundResponse response = myManager.put(Entity.json(bool));
				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return null;
			}
		};
		try {
			delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}

	}

	@Override
	public String sendMessage(List<String> recipients, String subject, String body, List<String> attachments) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(recipients, "recipients cannot be null");
				Objects.requireNonNull(subject, "subject cannot be null");
				Objects.requireNonNull(body, "body cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path("send");
				MultivaluedMap<String, Object> formData = new MultivaluedHashMap<String, Object>();

				recipients.forEach(r-> formData.add("to[]", r));
				formData.add("subject", subject);
				formData.add("body", body);
				if (attachments!=null) 
					attachments.forEach(a -> formData.add("attachments[]", a));
				
				GXInboundResponse response = myManager.post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));
				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}

	}


	@Override
	public void delete(String id) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(id, "id cannot be null");
				GXWebTargetAdapterRequest myManager = manager.path(id);

				GXInboundResponse response = myManager.delete();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return null;
			}
		};
		try {
			delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}

	}

	
	public List<? extends Item> getAttachments(String messageId) throws StorageHubException{
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest,  ItemList>() {
			@Override
			public  ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path(messageId).path("attachments");
				GXInboundResponse response = myManager.get();
				
				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
						throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return response.getSource().readEntity(ItemList.class);
			}
		};
		try {
			return delegate.make(call).getItemlist();
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}
	
	
	private List<Message> getMessages(final String path, int reduceBody ) throws StorageHubException{
		Call<GXWebTargetAdapterRequest, List<Message>> call = new Call<GXWebTargetAdapterRequest,  List<Message>>() {
			@Override
			public  List<Message> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path(path);
				if (reduceBody>0)
					myManager.queryParams(Collections.singletonMap("reduceBody", new Object[]{reduceBody}));
				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
						throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return response.getSource().readEntity(MessageList.class).getMessages();
			}
		};
		try {
			return delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

}
