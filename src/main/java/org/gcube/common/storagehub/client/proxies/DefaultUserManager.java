package org.gcube.common.storagehub.client.proxies;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.IdNotFoundException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;

public class DefaultUserManager extends DefaultManagerClient implements UserManagerClient {

	

	public DefaultUserManager(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		super(delegate);
	}

	@Override
	public void createUser(String userId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				MultivaluedMap<String, Object> formData = new MultivaluedHashMap<String, Object>();
				formData.add("user", userId);
				formData.add("password", userId+"pwd");

				GXInboundResponse response = myManager.post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
						throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError("error is "+response.getHTTPCode());
				}


				return null;
			}
		};
		try {
			delegate.make(call);
			return;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public void removeUser(String userId) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				try {
				
					GXInboundResponse response = myManager.path(userId).delete();
				
					if (response.isErrorResponse()) {
						if (response.hasException()) 
							throw response.getException();
						else if (response.getHTTPCode()==403)
							throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
						else
							throw new BackendGenericError();
					}
				}catch (Exception e) {
					e.printStackTrace();
					throw e;
				}

				return null;
			}
		};
		try {
			delegate.make(call);
			return;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public List<String> getUsers() throws StorageHubException {
		Call<GXWebTargetAdapterRequest, List<String>> call = new Call<GXWebTargetAdapterRequest,  List<String>>() {
			@Override
			public  List<String> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else if (response.getHTTPCode()==403)
						throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					else
						throw new BackendGenericError();
				}


				return response.getSource().readEntity(List.class);
			}
		};
		try {
			List<String> users = delegate.make(call);
			return users;
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}
	
	@Override
	public boolean exists(String user) throws StorageHubException {
		Call<GXWebTargetAdapterRequest, Boolean> call = new Call<GXWebTargetAdapterRequest,  Boolean>() {
			@Override
			public  Boolean call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path(user);

				GXInboundResponse response = myManager.get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						if (response.getException().getClass().equals(IdNotFoundException.class))
							return false;
						else throw response.getException();
					else if (response.getHTTPCode()==403)
						throw new UserNotAuthorizedException("the call to this method is not allowed for the user");
					
					else 
						throw new BackendGenericError();
				}

				return true;
			}
		};
		try {
			return delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}


}
