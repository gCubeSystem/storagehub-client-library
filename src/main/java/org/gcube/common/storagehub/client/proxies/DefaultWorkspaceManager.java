package org.gcube.common.storagehub.client.proxies;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.expressions.OrderField;
import org.gcube.common.storagehub.model.expressions.SearchableItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.query.Query;
import org.gcube.common.storagehub.model.service.ItemList;
import org.gcube.common.storagehub.model.service.ItemWrapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DefaultWorkspaceManager extends DefaultManagerClient implements WorkspaceManagerClient {

	public DefaultWorkspaceManager(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		super(delegate);
	}

	@Override
	public <T extends Item> T getWorkspace(String ... excludeNodes) {
		Call<GXWebTargetAdapterRequest, ItemWrapper<T>> call = new Call<GXWebTargetAdapterRequest, ItemWrapper<T>>() {
			@Override
			public ItemWrapper<T> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;

				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes !=null && excludeNodes.length>0)
					params.put("exclude",excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(ItemWrapper.class);
			}
		};
		try {
			ItemWrapper<T> result = delegate.make(call);
			return result.getItem();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	/*
	@Override
	public <T extends Item> T retieveItemByPath(String relativePath, String... excludeNodes) {
		Call<GXWebTargetAdapterRequest, ItemWrapper<T>> call = new Call<GXWebTargetAdapterRequest, ItemWrapper<T>>() {
			@Override
			public ItemWrapper<T> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager;
				if (excludeNodes !=null && excludeNodes.length>0)
					myManager =  myManager.queryParam("exclude",excludeNodes);

				myManager = manager.queryParam("relPath", relativePath);
				Invocation.Builder builder = myManager.request(MediaType.APPLICATION_JSON);
				ItemWrapper<T> response = builder.get(ItemWrapper.class);
				return response;
			}
		};
		try {
			ItemWrapper<T> result = delegate.make(call);
			return result.getItem();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}*/

	@Override
	public List<? extends Item> getVreFolders(String ... excludeNodes) {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("vrefolders");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes !=null && excludeNodes.length>0)
					params.put("exclude",excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(ItemList.class);
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<? extends Item> getVreFolders(int start, int limit, String ... excludeNodes) {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("vrefolders").path("paged");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes !=null && excludeNodes.length>0)
					params.put("exclude",excludeNodes);

				params.put("start", new Object[] {start});
				params.put("limit", new Object[] {limit});

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}


				return response.getSource().readEntity(ItemList.class);

			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public <T extends Item>  T getVreFolder(String ... excludeNodes) {
		Call<GXWebTargetAdapterRequest, ItemWrapper<T>> call = new Call<GXWebTargetAdapterRequest, ItemWrapper<T>>() {
			@Override
			public ItemWrapper<T> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("vrefolder");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes !=null && excludeNodes.length>0)
					params.put("exclude",excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(ItemWrapper.class);
			}
		};
		try {
			ItemWrapper<T> result = delegate.make(call);
			return result.getItem();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<? extends Item>  getRecentModifiedFilePerVre() {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("vrefolder").path("recents");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}


				return response.getSource().readEntity(ItemList.class);
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}


	@Override
	public <T extends Item>  T getTrashFolder(String ... excludeNodes) {
		Call<GXWebTargetAdapterRequest, ItemWrapper<T>> call = new Call<GXWebTargetAdapterRequest, ItemWrapper<T>>() {
			@Override
			public ItemWrapper<T> call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("trash");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes !=null && excludeNodes.length>0)
					params.put("exclude",excludeNodes);

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(ItemWrapper.class);
			}
		};
		try {
			ItemWrapper<T> result = delegate.make(call);
			return result.getItem();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void emptyTrash() throws StorageHubException, BackendGenericError {
		Call<GXWebTargetAdapterRequest, Void> call = new Call<GXWebTargetAdapterRequest, Void>() {
			@Override
			public Void call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("trash").path("empty");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).delete();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return null;
			}
		};
		try {
			delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String restoreFromTrash(String trashedItemid, String destinationFolderId) throws StorageHubException, BackendGenericError{
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.nonNull(trashedItemid);
				GXWebTargetAdapterRequest myManager = manager.path("trash").path("restore");

				MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
				formData.add("trashedItemId", trashedItemid);
				if (destinationFolderId !=null)
					formData.add("destinationId", destinationFolderId);


				GXInboundResponse response = myManager.queryParams(initQueryParameters()).put(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}				

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<? extends Item> search(Query<SearchableItem<?>> query, String ... excludeNodes) {
		Call<GXWebTargetAdapterRequest, ItemList> call = new Call<GXWebTargetAdapterRequest, ItemList>() {
			@Override
			public ItemList call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("query");
				Map<String, Object[]> params = initQueryParameters();

				if (excludeNodes !=null && excludeNodes.length>0)
					params.put("exclude",excludeNodes);

				if (query.getOrderFields() !=null && query.getOrderFields().size() >0) {
					List<String> orders = new ArrayList<>();
					for (OrderField field :query.getOrderFields())
						orders.add(String.format("[%s]%s",field.getField().getName(),field.getMode().toString()));
					params.put("o", orders.toArray(new Object[orders.size()]));
				}

				params.put("n", new Object[] {query.getSearchableItem().getNodeValue()});

				if (query.getLimit()!=-1)
					params.put("l", new Object[] { query.getLimit()});

				if (query.getOffset()!=-1)
					params.put("f", new Object[] { query.getOffset()});



				ObjectMapper mapper = new ObjectMapper();
				String serializedJson = mapper.writeValueAsString(query.getExpression());

				params.put("e", new Object[] { URLEncoder.encode(serializedJson)});

				GXInboundResponse response = myManager.queryParams(params).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(ItemList.class);
			}
		};
		try {
			ItemList result = delegate.make(call);
			return result.getItemlist();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long getTotalItemCount() {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("count");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {

			return Long.parseLong(delegate.make(call));
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long getTotalVolume() {
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				GXWebTargetAdapterRequest myManager = manager.path("size");

				GXInboundResponse response = myManager.queryParams(initQueryParameters()).get();

				if (response.isErrorResponse()) {
					if (response.hasException()) 
						throw response.getException();
					else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return Long.parseLong(delegate.make(call));
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String uploadFileToVolatile(InputStream stream, String fileName, long contentLength) throws StorageHubException{
		Call<GXWebTargetAdapterRequest, String> call = new Call<GXWebTargetAdapterRequest, String>() {
			@Override
			public String call(GXWebTargetAdapterRequest manager) throws Exception {
				Objects.requireNonNull(stream, "stream cannot be null");
				Objects.requireNonNull(fileName, "parentId cannot be null");
												
				GXWebTargetAdapterRequest myManager = manager.register(MultiPartFeature.class)
						.path("volatile");

				GXInboundResponse response =null;
				
				
				StreamDataBodyPart filePart = new StreamDataBodyPart("file", stream);
		        filePart.setContentDisposition(FormDataContentDisposition.name("file").fileName(fileName).size(contentLength).build());
		        
		        MultiPart multipartEntity = new FormDataMultiPart().bodyPart(filePart);
				
				response = myManager.queryParams(initQueryParameters()).post(Entity.entity(multipartEntity, multipartEntity.getMediaType()));

				if (response.isErrorResponse()) {
					if (response.hasException()) {
						throw response.getException();
					}else 
						throw new BackendGenericError("HTTP error code is "+response.getHTTPCode());
				}

				return response.getSource().readEntity(String.class);
			}
		};
		try {
			return delegate.make(call);
		}catch(StorageHubException e) {
			throw e;
		}catch(Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	@Override
	public String uploadFileToVolatile(InputStream stream, String fileName) throws StorageHubException {
		return this.uploadFileToVolatile(stream, fileName, -1);
	}

	@Override
	public String uploadFileToVolatile(File file) throws StorageHubException {
		if (!file.exists()) throw new BackendGenericError("file "+file.getAbsolutePath()+" doesn't exist");
		String fileName = file.getName();
			
		long size = file.length();
		try (InputStream is = new FileInputStream(file)){
			return uploadFileToVolatile(is, fileName, size);
		}catch (IOException io) {
			throw new BackendGenericError("error closing stream", io);
		}

	}



}
