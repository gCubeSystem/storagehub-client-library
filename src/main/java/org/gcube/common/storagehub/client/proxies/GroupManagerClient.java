package org.gcube.common.storagehub.client.proxies;

import java.util.List;

import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;

public interface GroupManagerClient extends ManagerClient{

	void addUserToGroup(String userId, String groupId) throws StorageHubException;
	
	void removeUserFromGroup(String userId, String groupId) throws StorageHubException;
	
	void removeGroup(String groupId) throws StorageHubException;
	
	void createGroup(String groupId, AccessType accessType, String folderOwner) throws StorageHubException;
	
	List<String> getUsersOfGroup(String groupId) throws StorageHubException;
	
	List<String> getGroups() throws StorageHubException;
	
	void addAdmin(String groupId, String userId) throws StorageHubException;
	
	void removeAdmin(String groupId, String userId) throws StorageHubException;
	
	List<String> getAdmins(String groupId) throws StorageHubException;
	
}
