package org.gcube.common.storagehub.client.proxies;

public interface ManagerClient {

	void register(Class<?> _classes);

	void impersonate(String user);
}

