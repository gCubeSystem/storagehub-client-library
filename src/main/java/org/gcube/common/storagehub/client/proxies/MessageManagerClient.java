package org.gcube.common.storagehub.client.proxies;

import java.util.List;

import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.messages.Message;

public interface MessageManagerClient extends ManagerClient{

	Message get(String id) throws StorageHubException;
	
	 List<? extends Item> getAttachments(String id) throws StorageHubException;

	List<Message> getReceivedMessages(int reduceBody) throws StorageHubException;

	List<Message> getSentMessages(int reduceBody) throws StorageHubException;
	
	List<Message> getReceivedMessages() throws StorageHubException;

	List<Message> getSentMessages() throws StorageHubException;

	
	void delete(String id) throws StorageHubException;

	String sendMessage(List<String> recipients, String subject, String body, List<String> attachments)
			throws StorageHubException;

	void setRead(String id, Boolean value) throws StorageHubException;

	void setOpened(String id, Boolean value) throws StorageHubException;
}
