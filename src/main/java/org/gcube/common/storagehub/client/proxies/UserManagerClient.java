package org.gcube.common.storagehub.client.proxies;

import java.util.List;

import org.gcube.common.storagehub.model.exceptions.StorageHubException;

public interface UserManagerClient extends ManagerClient{

	void createUser(String userId) throws StorageHubException;
	
	void removeUser(String userId) throws StorageHubException;
	
	List<String> getUsers() throws StorageHubException;

	boolean exists(String user) throws StorageHubException;
	
}
