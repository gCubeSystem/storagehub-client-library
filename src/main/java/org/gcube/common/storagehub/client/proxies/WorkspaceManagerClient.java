package org.gcube.common.storagehub.client.proxies;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.expressions.SearchableItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.query.Query;

public interface WorkspaceManagerClient extends ManagerClient{

	<T extends Item>  T getWorkspace(String ... excludeNodes);
	
	//<T extends Item>  T retieveItemByPath(String relativePath, String ... excludeNodes);
	
	List<? extends Item> getVreFolders(String ... excludeNodes);
	
	List<? extends Item> getVreFolders(int start, int limit, String ... excludeNodes);

	<T extends Item> T getVreFolder(String ... excludeNodes);

	List<? extends Item> search(Query<SearchableItem<?>> query, String ... excludeNodes);

	<T extends Item> T getTrashFolder(String ... excludeNodes);

	List<? extends Item> getRecentModifiedFilePerVre();

	String restoreFromTrash(String thrashedItemid, String destinationFolderId) throws StorageHubException, BackendGenericError;

	void emptyTrash() throws StorageHubException, BackendGenericError;
	
	long getTotalItemCount();
	
	long getTotalVolume();
	
	String  uploadFileToVolatile(InputStream stream, String filename, long contentLength) throws StorageHubException;
	
	String  uploadFileToVolatile(InputStream stream, String filename) throws StorageHubException;
	
	String  uploadFileToVolatile(File file) throws StorageHubException;
	
}
