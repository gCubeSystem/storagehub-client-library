package org.gcube.data.access.fs;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehub.client.dsl.ContainerType;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.gcube.common.storagehub.model.items.Item;
import org.junit.Test;

public class ImportTest {

	private static final String propFile = "/home/lucio/tokens.properties";

	private static final String devEnv = "dev-devvre";
	private static final String prodEnv = "prod-root";


	public void setUp(String env){

		try(InputStream is  = new FileInputStream(new File(propFile))){
			Properties prop = new Properties();
			prop.load(is);
			String value  =(String)prop.get(env);
			String[] splitValue = value.split(",");
			String token = splitValue[1];
			//SecretManagerProvider.instance.set(new GCubeSecret(token));

			SecurityTokenProvider.instance.set(token);
			ScopeProvider.instance.set(splitValue[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	@Test
	public void startImport() {
		try {
			//String folderId = "1c3bf96a-1143-48f8-8421-da176c87a016";
			String folderId = "684614df-c742-4aec-8a34-411a594dc85c";
			
			importTest(folderId, null);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void importTest(String folderId, String relativePath) throws Exception {
		setUp(prodEnv);

		StorageHubClient shc = new StorageHubClient();
		FolderContainer fc = shc.open(folderId).asFolder();
		List<ItemContainer<? extends Item>> items = fc.list().includeHidden().withContent().getContainers();

		System.out.println("folder "+folderId+" is with "+items.size());
		for (ItemContainer<? extends Item> itemContainer : items) {
			if (itemContainer.getType() == ContainerType.FOLDER) {
				new Thread(()-> createInDev(itemContainer.get(), itemContainer.getType(), relativePath, null)).run();
				setUp(prodEnv);
				importTest(itemContainer.getId(), relativePath == null ? itemContainer.get().getName() : relativePath+"/"+itemContainer.get().getName() );
			} else if (itemContainer.getType() == ContainerType.FILE){
				setUp(prodEnv);
				final URL publicLink = ((FileContainer)itemContainer).getPublicLink();
				System.out.println("publik Link "+publicLink.toString());
				new Thread(()-> createInDev(itemContainer.get(), itemContainer.getType(), relativePath, publicLink)).run();
			}
		}


	}


	public <T extends Item> void createInDev(T item, ContainerType type,  String relativePath, URL publicLink){
		setUp(devEnv);
		StorageHubClient shc = new StorageHubClient();
		try {
			FolderContainer currentFolder = shc.openVREFolder().openByRelativePath("testLucio").asFolder();
			if (relativePath!=null) {
				String[] paths = relativePath.split("/");
				for (String path : paths) 
					currentFolder = currentFolder.openByRelativePath(path).asFolder();
			}

			if (type == ContainerType.FOLDER) {
				currentFolder.newFolder(item.getName(), item.getDescription());
			} else if (type == ContainerType.FILE) {
				try ( InputStream fileIS = publicLink.openStream()){
					System.out.println("itemId is "+item.getId()) ;
					currentFolder.uploadFile(fileIS, item.getName(), item.getDescription(), ((AbstractFileItem) item).getContent().getSize());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
