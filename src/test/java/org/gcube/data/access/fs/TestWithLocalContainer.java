package org.gcube.data.access.fs;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.client.dsl.VREFolderManager;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestWithLocalContainer {

	private static StorageHubClient client;

	private static Logger log = LoggerFactory.getLogger(TestWithLocalContainer.class);

	public static URI storagehubUri;

	
	
	@BeforeClass
	public static void initialize() throws Exception{
		storagehubUri = new URL(String.format("http://%s:%d/storagehub", "localhost",8081)).toURI();
		/*AccessTokenSecret secret = new AccessTokenSecret("eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE2NDQyNDM4ODUsImlhdCI6MTY0NDI0MzU4NSwiYXV0aF90aW1lIjoxNjQ0MjQzNTg1LCJqdGkiOiI1NWQ4ZDc5OS1kNTIzLTQ0YmEtYTRkMC1iZjIyYzVlMTg4NzQiLCJpc3MiOiJodHRwczovL2FjY291bnRzLmRldi5kNHNjaWVuY2Uub3JnL2F1dGgvcmVhbG1zL2Q0c2NpZW5jZSIsImF1ZCI6IiUyRmdjdWJlIiwic3ViIjoiNGMxMWRlODQtZGRjOS00ZGQxLWI5N2EtZWE4MmQyZDEzOGE2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibmV4dC5kNHNjaWVuY2Uub3JnIiwic2Vzc2lvbl9zdGF0ZSI6ImZkZTA3MGE2LTVkOTUtNDc3Ni1hMTFiLTBhZTI1MzQ5NGQyMyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiLyoiXSwicmVzb3VyY2VfYWNjZXNzIjp7IiUyRmdjdWJlIjp7InJvbGVzIjpbIkluZnJhc3RydWN0dXJlLU1hbmFnZXIiLCJNZW1iZXIiXX19LCJhdXRob3JpemF0aW9uIjp7InBlcm1pc3Npb25zIjpbeyJyc2lkIjoiMTU5ZDcyMDQtNjlmYS00ZmY0LTlhOTQtMzVlMWUyMzA5MDQyIiwicnNuYW1lIjoiRGVmYXVsdCBSZXNvdXJjZSJ9XX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiTHVjaW8gTGVsaWkiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJsdWNpby5sZWxpaSIsImdpdmVuX25hbWUiOiJMdWNpbyIsImZhbWlseV9uYW1lIjoiTGVsaWkiLCJlbWFpbCI6Imx1Y2lvLmxlbGlpQGlzdGkuY25yLml0In0.SGuGXnY63WJ45tdypywaopO--FCxY6ZWAX74MGVw_zdNMMQI5zgtGfhZQewFWQ6HhVINDStXqvdSDluhF9JwFXCUGvxB2BipfKQYsAQnm3I4K3kgMl26kuQIL5Im-apw2XWvImTgLshX1AzpyqzFR1Jpf_p65GCMpcHLEwo_nTGLkrZNtfRdtqakZ8S8lowGaV2J2mNf4AHhtARSLJF3GqwynF5pPUXR2iOMUrjw_FLnIuPMfAjk_jHMeYKIxXNBLsoEpELju3pr2gEkv1ZRH9bZkyg7_3hW4mGzxq_Ea9w1FBVJOlkp6vypuISLNyJZcVOnBa4Dg1KRxxlp3aZ6vA");
		SecretManagerProvider.instance.set(secret);*/
		client = new StorageHubClient(storagehubUri);
		createUser();
	}

	@Before
	public void reset() throws Exception {
		/*CredentialSecret secret = new CredentialSecret("sg4-test-client", "a156a7db-3b32-4cd5-b27b-2488e0e01698", "/gcube");
		SecretManagerProvider.instance.set(secret);*/  
	}

	private static void createUser() throws Exception{
		client.createUserAccount("test.user");
		client.createUserAccount("test.user2");
		VREFolderManager vremanager = client.getVreFolderManager("gcube-devVre-myvre");
		vremanager.createVRE(AccessType.WRITE_OWNER, "test.user");
		vremanager.addUser("test.user2");
	}

	//impersonating test.user
	@Test
	public void uploadFile() throws Exception{

		client.impersonate("test.user");
		FolderContainer vreFolder = (FolderContainer) client.getVREFolders().getContainers().stream().findFirst().get();
		try(InputStream stream = this.getClass().getResourceAsStream("/output.xlsx")){
			vreFolder.uploadFile(stream, "userTestfile2", "userTestfile" );
		}

		assertTrue(vreFolder.list().getItems().size()>0);
		
		
	}

}


